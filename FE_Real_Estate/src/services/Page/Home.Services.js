import api from '../services'

const GetListProductServices = {
   getListProduct: () => {
      return (
         api
            .call()
            .get(`/home`)
      )
   },
   postNewsStatus: ({
      title,
      address,
      content,
      prices,
      beds,
      baths,
      images,
      description,
      valueCurrent,
      valueName,
      valueArea,
      unit,
      phoneNumber,
      idUser,
      nameUser
   }) => {
      console.log("title:", title)

      return (
         api
            .call()
            .post('/postNews', {
               "txtKind": valueCurrent,
               "txtName": valueName,
               "txtTiltle": title,
               "fileImage": images,
               "txtDescription": description,
               "txtPrice": parseInt(prices),
               "txtUnit": unit,
               "txtSize": parseInt(valueArea),
               "txtContent": content,
               "txtAdress": address,
               "active": true,
               "txtBath": parseInt(baths),
               "txtBad": parseInt(beds),
               "txtName_user": nameUser,
               "txtPhone": parseInt(phoneNumber),
               "id_post": idUser
            })
      )
   }
}

export default GetListProductServices