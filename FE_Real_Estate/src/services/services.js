import axios from 'axios'

const BASE_URL = 'http://192.168.1.123:4000';

const api = {
   call() {
      return axios.create({
         baseURL: BASE_URL,
         headers: {
            'Content-Type': 'application/json'
         }
      })
   },
}
export default api