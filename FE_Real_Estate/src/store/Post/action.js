import { NAME_ACTION } from '../../constants'
import { GetDataHome } from '../../services/Page'
export const actGetPostDetail = (postDetail) => {
   return {
      type: NAME_ACTION.GET_POST_DETAIL,
      payload: {
         postDetail
      }
   }
}

export const actGetListPost = (listPost) => {
   return {
      type: NAME_ACTION.GET_LIST_POST,
      payload: {
         listPost
      }
   }
}
export const asyncPostNewsStatus = ({
   title,
   address,
   content,
   prices,
   beds,
   baths,
   images,
   description,
   valueCurrent,
   valueName,
   valueArea,
   unit,
   phoneNumber,
   idUser,
   nameUser
}) => {
   return async (dispatch) => {
      try {
         const response = await GetDataHome.postNewsStatus({
            title,
            address,
            content,
            prices,
            beds,
            baths,
            images,
            description,
            valueCurrent,
            valueName,
            valueArea,
            unit,
            phoneNumber,
            idUser,
            nameUser
         })
         if (response.data.status === 200) {
            console.log("response", response);
            return {
               ok: true
            }
         }
      } catch (error) {
         return {
            ok: false
         }
      }
   }
}

export const asyncGetListPost = () => {
   return async (dispatch) => {
      try {
         const response = await GetDataHome.getListProduct()
         if (response.status === 200) {
            const listPost = response.data
            const listPostActive = listPost.filter((item) => {
               return item.active === true
            })
            dispatch(actGetListPost(listPostActive))
            return {
               ok: true
            }
         } else {
            return {
               ok: false
            }
         }

      } catch (error) {
         console.log("Error:", error);
         return {
            ok: false
         }
      }
   }
}