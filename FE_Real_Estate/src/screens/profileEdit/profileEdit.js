import React, { useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, Image, TextInput } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import Modal from 'react-native-modal'


import { StyleProfileEdit } from '../../assets/styles/profileEdit'
import IconAntd from 'react-native-vector-icons/AntDesign'

export default function ProfileEdit({ navigation }) {

   const user = useSelector(state => state.Auth.inforUserLogin)

   const [inforUser, setInforUser] = useState({
      fullname: user.fullname
   })
   const [isShow, setIsShoW] = useState(false)


   const handleBack = () => {
      navigation.goBack();
   }
   const hanldeSubmit = () => {
      console.log('====================================');
      console.log("dasdsad");
      console.log('====================================');
   }
   const handleCamera = () => {
      setIsShoW(true)
   }
   const handleCloseModal = () => {
      setIsShoW(false)
   }
   return (
      <View style={StyleProfileEdit.container}>
         <StatusBar barStyle="dark-content" translucent backgroundColor="#55efc400" />
         <View style={StyleProfileEdit.header}>
            <TouchableOpacity style={StyleProfileEdit.btnBack} onPress={handleBack}>
               <IconAntd name="arrowleft" style={StyleProfileEdit.arrowBack} />
            </TouchableOpacity>
            <TouchableOpacity style={StyleProfileEdit.btnCheck} onPress={hanldeSubmit}>
               <IconAntd name="check" style={StyleProfileEdit.arrowBack} />
            </TouchableOpacity>
         </View>
         <View style={StyleProfileEdit.body}>
            <View style={StyleProfileEdit.avatarUser}>
               <Image style={StyleProfileEdit.avatar} source={{ uri: user && user.img }} />
               <TouchableOpacity style={StyleProfileEdit.btnCamera} onPress={handleCamera}>
                  <IconAntd size={30} color="#7f8c8d" name="camerao" />
               </TouchableOpacity>
            </View>
            <View style={StyleProfileEdit.changeInput}>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>Fullname</Text>
                  <TextInput
                     style={StyleProfileEdit.input}
                     value={inforUser.fullname}
                  />
               </View>
               {/* <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>Username</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>Password</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>New Password</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View> */}
            </View>
            <Modal
               style={StyleProfileEdit.modalCamera}
               isVisible={isShow}
               onBackdropPress={handleCloseModal}
            >
               <View style={StyleProfileEdit.CameraView}>
                  <TouchableOpacity style={StyleProfileEdit.option}>
                     <Image style={StyleProfileEdit.img} source={require('../../assets/images/camera.png')} />
                     <Text style={StyleProfileEdit.textOption}>Camera</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={StyleProfileEdit.option}>
                     <Image style={StyleProfileEdit.img} source={require('../../assets/images/image.png')} />
                     <Text style={StyleProfileEdit.textOption}>Galley</Text>
                  </TouchableOpacity>
               </View>
            </Modal>
         </View>
      </View>
   )
}