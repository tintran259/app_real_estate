import React, { useState } from 'react'
import { View, Text, StatusBar, TouchableOpacity, Image, TextInput, Alert } from 'react-native'
import { useDispatch } from 'react-redux'
import Modal from 'react-native-modal'


import IconAntd from 'react-native-vector-icons/AntDesign'
import { asyncRegister } from '../../store/Login/action'

import { StylesRegister } from '../../assets/styles/Register'
export default function RegisterScreens({ navigation }) {

   const dispatch = useDispatch();
   const [formRegister, setFormRegister] = useState({
      image: "https://st4.depositphotos.com/4329009/19956/v/600/depositphotos_199564354-stock-illustration-creative-vector-illustration-default-avatar.jpg",
      fullname: "",
      username: "",
      password: "",
      rePassword: "",
   })
   const [isShowModal, setIsShowModal] = useState(false)

   const handleBack = () => {
      navigation.goBack();
   }
   const handleChangeAvatar = () => {
      setFormRegister({
         ...formRegister,
         image: "https://ict-imgs.vgcloud.vn/2020/09/01/19/huong-dan-tao-facebook-avatar.jpg"
      })
   }
   const validate = (fullname, username, password) => {
      if (password.trim("") === "") {
         return false
      }
      else if (fullname.trim("") === "") {
         return false
      }
      else if (username.trim("") === "") {
         return false
      }
      else if (formRegister.rePassword.trim("") === "") {
         return false
      }
      else if (password.trim("") !== formRegister.rePassword.trim("")) {
         return false
      }
      return true
   }
   const handleSubmitRegiser = () => {
      const { fullname, username, password, image } = formRegister
      if (validate(fullname, username, password)) {
         dispatch(asyncRegister({ username, password, fullname, image }))
            .then((res) => {
               console.log("res Register:", res);
            })
      } else {
         setIsShowModal(true)
      }
   }
   const handleCloseModal = () => {
      setIsShowModal(false)
   }
   return (
      <View style={StylesRegister.container}>
         <StatusBar barStyle="light-content" translucent backgroundColor="#53b5b6" />
         <View style={StylesRegister.header}>
            <TouchableOpacity style={StylesRegister.btnBack} onPress={handleBack}>
               <IconAntd style={StylesRegister.iconBack} name="arrowleft" />
            </TouchableOpacity>
            <Text style={StylesRegister.titleHeader}>Register</Text>
         </View>
         <View style={StylesRegister.FormRegister}>
            <View style={StylesRegister.headerForm}>
               <TouchableOpacity onPress={handleChangeAvatar}>
                  <Image style={StylesRegister.avatar} source={{ uri: formRegister.image }} />
               </TouchableOpacity>
            </View>
            <View style={StylesRegister.bodyForm}>
               <View style={StylesRegister.inputForm}>
                  <Text style={StylesRegister.label}>Fullname</Text>
                  <TextInput
                     value={formRegister.fullname}
                     onChangeText={text => setFormRegister({ ...formRegister, fullname: text })}
                     placeholder="Fullname ..."
                     style={StylesRegister.input}></TextInput>
               </View>
               <View style={StylesRegister.inputForm}>
                  <Text style={StylesRegister.label}>Username</Text>
                  <TextInput
                     value={formRegister.username}
                     onChangeText={text => setFormRegister({ ...formRegister, username: text })}
                     placeholder="Username ..."
                     style={StylesRegister.input}></TextInput>
               </View>
               <View style={StylesRegister.inputForm}>
                  <Text style={StylesRegister.label}>Password</Text>
                  <TextInput
                     value={formRegister.password}
                     onChangeText={text => setFormRegister({ ...formRegister, password: text })}
                     placeholder="Password ..."
                     secureTextEntry={true}
                     style={StylesRegister.input}></TextInput>
               </View>
               <View style={StylesRegister.inputForm}>
                  <Text style={StylesRegister.label}>Re-Password</Text>
                  <TextInput
                     secureTextEntry={true}
                     value={formRegister.rePassword}
                     onChangeText={text => setFormRegister({ ...formRegister, rePassword: text })}
                     placeholder="Re-Password ..."
                     style={StylesRegister.input}></TextInput>
               </View>
            </View>
            <View style={StylesRegister.footer}>
               <TouchableOpacity style={StylesRegister.btnRegister} onPress={handleSubmitRegiser}>
                  <Text style={StylesRegister.textRegister}>Create Account</Text>
               </TouchableOpacity>
            </View>
         </View>
         <Modal
            style={{ alignItems: "center" }}
            isVisible={isShowModal}
            onBackdropPress={handleCloseModal}
         >
            <View style={StylesRegister.modal}>
               <View style={StylesRegister.headerModal}>
                  <Text style={StylesRegister.titleOops}>Oops</Text>
                  <Text style={StylesRegister.textModal}>
                     {`Please enter `}
                  </Text>
               </View>
               <TouchableOpacity style={StylesRegister.btnOk} onPress={handleCloseModal}>
                  <Text style={StylesRegister.textModalS}>OK</Text>
               </TouchableOpacity>
            </View>
         </Modal>
      </View>
   )
}