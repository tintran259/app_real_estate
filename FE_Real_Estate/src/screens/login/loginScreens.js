import React from 'react'
import { View, Text, Image, StatusBar, ImageBackground } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { FormLogin } from '../../components/loginScreens'
import stylesIndexLogin from '../../assets/styles/indexLogin.style'
export default function LoginScreens({ navigation }) {
   return (
      <View style={stylesIndexLogin.container}>
         <StatusBar barStyle="light-content" translucent backgroundColor="#55efc400" />
         <View style={stylesIndexLogin.header}>
            <Image
               style={stylesIndexLogin.background}
               source={require('../../assets/images/Splash.jpg')}
               resizeMode="cover"
            />
            <Text numberOfLines={2} style={stylesIndexLogin.titleHeader}>
               Wellcome
            </Text>
         </View>
         <FormLogin navigation={navigation} />
      </View>
   )
}