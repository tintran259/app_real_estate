import React from 'react'
import { View, Text, StatusBar, TextInput, TouchableOpacity } from 'react-native'

import { StyleProfileEdit } from '../../assets/styles/profileEdit'
import IconAntd from 'react-native-vector-icons/AntDesign'
export default function ChangePassword({ navigation }) {
   const handleBack = () => {
      navigation.goBack();
   }
   const hanldeSubmit = () => {
      console.log("hanldeSubmit");
   }
   return (
      <View style={StyleProfileEdit.container}>
         <View style={StyleProfileEdit.header}>
            <TouchableOpacity style={StyleProfileEdit.btnBack} onPress={handleBack}>
               <IconAntd name="arrowleft" style={StyleProfileEdit.arrowBack} />
            </TouchableOpacity>
            <TouchableOpacity style={StyleProfileEdit.btnCheck} onPress={hanldeSubmit}>
               <IconAntd name="check" style={StyleProfileEdit.arrowBack} />
            </TouchableOpacity>
         </View>
         <StatusBar barStyle="dark-content" translucent backgroundColor="#55efc400" />
         <View style={StyleProfileEdit.body}>
            <View style={StyleProfileEdit.changeInput}>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>Username</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>Old-Password</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View>
               <View style={StyleProfileEdit.inputFullname}>
                  <Text style={StyleProfileEdit.titleLabel}>New-Password</Text>
                  <TextInput style={StyleProfileEdit.input} />
               </View>
            </View>
         </View>
      </View>
   )
}