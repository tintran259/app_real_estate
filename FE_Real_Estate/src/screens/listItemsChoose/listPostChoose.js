import React from 'react'
import { Text, View, Button } from 'react-native'
import { StylesListPostChoose } from '../../assets/styles/listPostChoose'
import IconAntd from 'react-native-vector-icons/AntDesign'



export default function ListPostChoose({ navigation }) {
   return (
      <View>
         <View style={StylesListPostChoose.header} >
            <IconAntd style={StylesListPostChoose.iconHeader} name="heart" size={25} />
            <Text style={StylesListPostChoose.titleHeader}>Danh sách đã thích</Text>
         </View>
         <Text> ListPostChoose</Text>
         <Button onPress={() => navigation.navigate('ListDetail')} title="List Detail" />
         <Text></Text>
         <Button onPress={() => navigation.navigate('ProfileDetail')} title="Profile Detail" />
      </View>
   )
}