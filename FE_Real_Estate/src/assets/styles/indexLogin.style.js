import { StyleSheet } from 'react-native'

const stylesIndexLogin = StyleSheet.create({
   container: {
      flex: 1,
      backgroundColor: "#53b5b6"
   },
   header: {
      width: "100%",
      height: 280,
      alignItems: "center",
      justifyContent: "center",
      // borderBottomLeftRadius: 50,
      // backgroundColor: "#fff"
   },
   titleHeader: {
      fontSize: 26,
      fontWeight: "bold",
      color: "#fff",
      letterSpacing: 1,
   },
   bg: {
      position: "absolute",
      width: 400,
      height: 400,
      top: 270,
      opacity: 0.5
   },
   background: {
      width: 100,
      height: 100,
      position: "relative",
      borderBottomLeftRadius: 50,
   }
})

export default stylesIndexLogin