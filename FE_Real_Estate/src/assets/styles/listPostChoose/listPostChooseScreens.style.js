import { StyleSheet } from 'react-native'


const StylesListPostChoose = StyleSheet.create({
   header: {
      width: "100%",
      height: 50,
      backgroundColor: "#fff",
      borderBottomWidth: 1,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center"
   },
   titleHeader: {
      fontSize: 22,
      fontWeight: "bold",
      color: "#e74c3c"
   },
   iconHeader: {
      position: "absolute",
      left: 70,
      color: "#e74c3c"
   }
})

export default StylesListPostChoose