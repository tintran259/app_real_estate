import { StyleSheet } from 'react-native'

const StylesRegister = StyleSheet.create({
   container: {
      flex: 1,
      marginTop: "6%",
      backgroundColor: "#fff"
   },
   header: {
      width: "100%",
      height: 40,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#53b5b6"
   },
   iconBack: {
      fontSize: 20,
      color: "#fff",

   },
   titleHeader: {
      color: "#fff",
      fontSize: 16,
      fontWeight: "bold",
   },
   btnBack: {
      width: 50,
      height: 40,
      position: "absolute",
      top: 10,
      left: "5%",
   },
   avatar: {
      width: 120,
      height: 120,
      borderRadius: 15
   },
   headerForm: {
      marginTop: 10,
      width: "100%",
      height: 120,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "flex-end"
   },
   btnCam: {
      position: "absolute",
      right: "18%",
      width: 50,
      height: 40,
      justifyContent: "flex-end",
      alignItems: "center"
   },
   iconCam: {
      fontSize: 23,
      color: "#fff"
   },
   bodyForm: {
      width: "100%",
      padding: 10
   },
   label: {
      color: "#636e72",
      letterSpacing: 1,
      fontWeight: "bold",
      marginLeft: 10
   },
   input: {
      width: "100%",
      height: 40,
      borderRadius: 10,
      borderBottomWidth: 1,
      borderColor: "#b2bec3",
      paddingHorizontal: 10,
      marginTop: 10
   },
   inputForm: {
      marginBottom: 20
   },
   footer: {
      width: "100%",
      justifyContent: "center",
      alignItems: "center"
   },
   btnRegister: {
      width: "80%",
      height: 40,
      backgroundColor: "#53b5b6",
      borderRadius: 10,
      justifyContent: "center",
      alignItems: "center"
   },
   textRegister: {
      color: "#FFF",
      fontWeight: "bold",
      letterSpacing: 1
   },
   modal: {
      width: "80%",
      height: 150,
      backgroundColor: "#fff",
      borderRadius: 15,
      alignItems: "center"
   },
   headerModal: {
      width: "100%",
      height: 100,
      alignItems: "center",
      paddingHorizontal: 10,
      borderBottomColor: "#dfe6e9",
      borderBottomWidth: 1
   },
   titleOops: {
      fontSize: 20,
      marginTop: 20,
      fontWeight: "bold",
      color: "#2d3436",
      letterSpacing: 1
   },
   textModal: {
      color: "#636e72",
      marginTop: 10
   },
   btnOk: {
      width: 200,
      height: 45,
      justifyContent: "center",
      alignItems: "center"
   },
   textModalS: {
      color: "#3498db",
      fontWeight: "bold"
   }
})

export { StylesRegister }