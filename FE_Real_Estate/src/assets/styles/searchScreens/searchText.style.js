import { StyleSheet } from 'react-native'


const StylesSearchText = StyleSheet.create({
   inputSearch: {
      width: "98%",
      height: 40,
      borderWidth: 1,
      borderRadius: 15,
      paddingLeft: 20,
      backgroundColor: "#fff",
      paddingRight: 50,
      borderColor: "#b2bec3",
      color: "#2d3436",
      fontSize: 13
   },
   headerSearchText: {
      width: "100%",
      height: 50,
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center"
   },
})

export default StylesSearchText