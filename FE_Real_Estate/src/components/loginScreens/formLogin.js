import React, { useEffect, useState } from 'react'
import {
   View,
   TextInput,
   Text,
   TouchableOpacity,
   Image
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import Modal from 'react-native-modal'

import IconEvil from 'react-native-vector-icons/EvilIcons'
import IconFeather from 'react-native-vector-icons/Feather'
import stylesFormLogin from '../../assets/styles/formLogin.style'
import { StyleModalSuccessed } from '../../assets/styles/ModalStyle'

import { asyncLoginForm, actLoginSuccess } from '../../store/Login/action'



export default function FormLogin({ navigation }) {

   const dispatch = useDispatch()

   const [formLogin, setFormLogin] = useState({
      username: "",
      password: ""
   })
   const inforUserLogin = useSelector(state => state.Auth.inforUserLogin)
   const [isShowPass, setIsShowPass] = useState(true)
   const [isUsernameEmpty, setIsUsernameEmpty] = useState(false)
   const [isUsername, setIsUsername] = useState(false)
   const [isPassword, setIsPassword] = useState(false)
   const [isVisible, setIsvisible] = useState(false)
   const [isWrongPass, setIsWrongPass] = useState(false)
   useEffect(() => {
      setIsUsernameEmpty(false)
      setIsUsername(false)
      setIsPassword(false)
   }, [formLogin.username !== "" && formLogin.password !== ""])
   useEffect(() => {
      setTimeout(() => {
         setIsvisible(false)
      }, 1000)
   }, [isVisible === true])
   const handleShowPass = () => {
      setIsShowPass(false)
   }
   const handleHidePass = () => {
      setIsShowPass(true)
   }
   const handleRegister = () => {
      navigation.navigate('Register')
   }
   const handleBackdrop = () => {
      setIsvisible(false)
   }
   const validateForm = () => {
      if (formLogin.username.trim(" ") === "" && formLogin.password.trim(" ") === "") {
         setIsUsername(true)
         setIsPassword(true)
         setIsUsernameEmpty(true)
         return false
      }
      else if (formLogin.username.trim(" ") === "") {
         setIsUsername(true)
         setIsUsernameEmpty(true)
         return false
      }
      else if (formLogin.password.trim(" ") === "") {
         setIsPassword(true)
         setIsUsernameEmpty(true)
         return false
      }
      return true
   }

   const handleBackdropWrong = () => {
      setIsWrongPass(false)
   }

   const hanldeSubmit = () => {
      if (validateForm()) {
         const { username, password } = formLogin;
         dispatch(asyncLoginForm({ username, password }))
            .then((res) => {
               const dataUser = res.res;
               if (res.ok === true) {
                  setIsvisible(true)
                  dispatch(actLoginSuccess(dataUser))
               } else {
                  setIsWrongPass(true)
               }
            })
      }
   }
   //=====================================State
   //===============================Render
   return (
      <View style={stylesFormLogin.body}>
         <View style={[stylesFormLogin.Formlogin, { marginBottom: 40 }]}>
            <IconFeather name="user" style={[stylesFormLogin.icon, isUsername ? { borderBottomColor: "#e74c3c" } : { borderBottomColor: "#fff" }]} />
            <TextInput
               style={[stylesFormLogin.input, isUsername ? { borderBottomColor: "#e74c3c" } : { borderBottomColor: "#fff" }]}
               placeholder="Username ..."
               placeholderTextColor={isUsername ? "#e74c3c" : "#ecf0f1"}
               value={formLogin.username}
               onChangeText={text => setFormLogin({ ...formLogin, username: text })}
            />
         </View>
         <View style={[stylesFormLogin.Formlogin, { marginBottom: 10 }]}>
            <IconFeather name="lock" style={[stylesFormLogin.icon, isPassword ? { borderBottomColor: "#e74c3c" } : { borderBottomColor: "#fff" }]} />
            <TextInput
               placeholderTextColor={isPassword ? "#e74c3c" : "#ecf0f1"}
               style={[stylesFormLogin.input, isPassword ? { borderBottomColor: "#e74c3c" } : { borderBottomColor: "#fff" }]}
               secureTextEntry={isShowPass}
               placeholder="Password ..."
               value={formLogin.password}
               onChangeText={text => setFormLogin({ ...formLogin, password: text })}
            />
            {
               isShowPass ?
                  <TouchableOpacity style={stylesFormLogin.iconShowHidePass} onPress={handleShowPass}>
                     <IconFeather style={stylesFormLogin.iconshowPass} name="eye-off" />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={stylesFormLogin.iconShowHidePass} onPress={handleHidePass}>
                     <IconFeather style={stylesFormLogin.iconshowPass} name="eye" />
                  </TouchableOpacity>
            }
         </View>
         {
            isUsernameEmpty ?
               <Text style={{ fontStyle: "italic", color: "#e74c3c" }}> Please fill in the blanks !</Text>
               :
               <Text></Text>
         }
         <View style={stylesFormLogin.for}>
            <TouchableOpacity style={stylesFormLogin.forgotPass}>
               <Text style={stylesFormLogin.textForgot}>Forgot password ?</Text>
            </TouchableOpacity>
         </View>
         <View style={stylesFormLogin.footer}>
            <View style={stylesFormLogin.sigin}>
               <TouchableOpacity onPress={hanldeSubmit} style={stylesFormLogin.btnSignIn}>
                  <Text style={stylesFormLogin.textBtn}>Sign in</Text>
               </TouchableOpacity>
               <TouchableOpacity style={stylesFormLogin.btnFacebook}>
                  <IconEvil style={stylesFormLogin.logoFB} name="sc-facebook" />
               </TouchableOpacity>
               <TouchableOpacity style={stylesFormLogin.btnGmail}>
                  <IconEvil style={stylesFormLogin.logoFB} name="sc-google-plus" />
               </TouchableOpacity>
            </View>
            <Text style={stylesFormLogin.or}>-------------------- or ----------------------</Text>
            <TouchableOpacity onPress={handleRegister} style={stylesFormLogin.btnSignUp}>
               <Text style={stylesFormLogin.textBtn}>Sign up</Text>
            </TouchableOpacity>
         </View>
         <Modal
            style={{ flexDirection: "column", alignItems: "center" }}
            onBackdropPress={handleBackdrop}
            isVisible={isVisible}
            backdropOpacity={0.3}
         >
            <View style={StyleModalSuccessed.dialog}>
               <Image source={require('../../assets/images/checked.png')} />
               <Text style={StyleModalSuccessed.titleHeader}>Wellcome</Text>
               <Text style={StyleModalSuccessed.titleHeader}>{inforUserLogin && inforUserLogin.username}</Text>
            </View>
         </Modal>
         <Modal
            animationIn="shake"
            animationInTiming={1000}
            animationOut="zoomOut"
            animationOutTiming={1000}
            style={{ flexDirection: "column", alignItems: "center" }}
            isVisible={isWrongPass}
            backdropOpacity={0.3}
            onBackdropPress={handleBackdropWrong}
         >
            <View style={StyleModalSuccessed.viewWrongPass}>
               <Image style={StyleModalSuccessed.IMAGE} source={require('../../assets/images/abuse.png')} />
               <Text style={StyleModalSuccessed.textWrong}>uh-no... !</Text>
               <Text style={StyleModalSuccessed.textMay}>Maybe you were wrong username and password !</Text>
               <TouchableOpacity
                  onPress={handleBackdropWrong}
                  style={StyleModalSuccessed.btn_try}>
                  <Text style={StyleModalSuccessed.textTry}> Try Again</Text>
               </TouchableOpacity>
            </View>
         </Modal>
      </View>
   )
}