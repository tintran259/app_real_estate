const mongoose = require('mongoose');

const categrogySchema = new mongoose.Schema({
    name: String,
    kids: [{ type: mongoose.Schema.Types.ObjectId }],
    ordering: Number,
    active: Boolean
})
module.exports = mongoose.model("caterogy", categrogySchema);