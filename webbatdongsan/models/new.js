const mongoose = require('mongoose');

const newSchema = new mongoose.Schema({
    title: String,
    decription: String,
    image: String,
    content: String,
    ordering: Number,
    active: Boolean
})
module.exports = mongoose.model("new", newSchema);