var express = require('express');
var app = express();
var cors = require('cors');

const server = require('http').Server(app);
var io = require("socket.io")(server);

const mongoose = require('mongoose');

// app.set("view engine", "ejs");
// app.set("views", "./views");
app.use(express.static("public"));
app.use(cors());
//Search Vietnamese
const fullTextSearch = require('fulltextsearch');
var fullTextSearchVi = fullTextSearch.vi;
//Body parser
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//nodeMailer

//connect Mongoose
mongoose.connect('mongodb://localhost:27017/tintuc', { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
    if (!err) {
        console.log("thanh cong ");
    } else {
        console.log("that bai");
    }
});
//user
const homeRouter = require('./routers/user/home.router');
const sellRouter = require('./routers/user/sell.router');
const loginRouter = require('./routers/user/login.router');
const registerRouter = require('./routers/user/register.router');
const myAccountRouter = require('./routers/user/myAccont.router');
const myNewsRouter = require('./routers/user/myNews.router');
const postNewsRouter = require('./routers/user/postNews.router');
const sendMailRouter = require('./routers/user/sendMail.router');
const detailProductRouter = require('./routers/user/detailProduct.router')
//#region  User
//Home
app.use('/home', homeRouter);
//Sell
app.use('/sell', sellRouter);
//detailProduct
app.use('/detailProduct', detailProductRouter)
//login
app.use('/login', loginRouter);
//register
app.use('/register', registerRouter);
//myAcount
app.use('/myAccount', myAccountRouter);
//myNews
app.use('/myNews', myNewsRouter);
//postNews
app.use('/postNews', postNewsRouter);
//sendMail
app.use('/sendMail', sendMailRouter);
//#endregion User
//Admin
const login_adminRouter = require('./routers/admin/login_admin.router');
const accountManagerRouter = require('./routers/admin/accountManager.router');
const newsManagerRouter = require('./routers/admin/newsManager.router');
const { log } = require('console');
//#region admin
//loginAdmin
app.use('/login_admin', login_adminRouter);
// account manager
app.use('/accountManager', accountManagerRouter)
//newsManager
app.use('/newsManager', newsManagerRouter)
//#endregion


server.listen(4000, function () {
    console.log('Example app listening on port 4000!');
});

