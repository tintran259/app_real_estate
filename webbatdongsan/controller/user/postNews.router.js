var Detail = require('../../models/web/detail')

module.exports.postNews = async (req, res) => {
    var detail = new Detail({
        kind: req.body.txtKind,
        name: req.body.txtName,
        title: req.body.txtTiltle,
        image: req.body.fileImage,
        decription: req.body.txtDescription,
        price: req.body.txtUnit === "Thỏa Thuận" ? 0 : req.body.txtPrice,
        unit: req.body.txtUnit,
        size: req.body.txtSize,
        content: req.body.txtContent,
        adress: req.body.txtAdress,
        active: req.body.active,
        numberbath: req.body.txtBath,
        numberbed: req.body.txtBad,
        name_user: req.body.txtName_user,
        phone: req.body.txtPhone,
        id_post: req.body.id_post
    })
    await detail.save((err, data) => {
        if (err === null) {
            res.json({
                status: 200,
                message: "Success",
                data,
            })
        } else {
            console.log("Err:", err);
            res.json({
                status: 400,
                message: "Fail",
            })
        }

    })
}