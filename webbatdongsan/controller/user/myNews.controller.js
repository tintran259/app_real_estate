const Detail = require('../../models/web/detail')

module.exports.myNews = async (req, res) => {
    await Detail.find((err, data) => {
        res.send(data)
    }).then().catch( (err) => console.log(err))

}

module.exports.getEdit =  async (req, res) => {
    Detail.find((err, data) => {
        res.send(data)
    }).then().catch((err) => console.log(err))
}

module.exports.postEdit = async (req, res) => {
    //    console.log(req.body);
    if (req.body.fileImage === null) {
        await Detail.findByIdAndUpdate(req.body._id, {
            kind: req.body.txtKind,
            name: req.body.txtName,
            title: req.body.txtTiltle,
            image: req.body.oldImage,
            decription: req.body.txtDescription,
            price: req.body.txtPrice,
            unit: req.body.txtUnit,
            size: req.body.txtSize,
            content: req.body.txtContent,
            adress: req.body.txtAdress,
            active: req.body.active,
            numberbath: req.body.txtBath,
            numberbed: req.body.txtBad,
            name_user: req.body.txtName_user,
            phone: req.body.txtPhone,
            id_post: req.body.id_post

        }).then(console.log('thanh cong')).catch((err) => console.log(err))
    } else {
        await Detail.findByIdAndUpdate(req.body._id, {
            kind: req.body.txtKind,
            name: req.body.txtName,
            title: req.body.txtTiltle,
            image: req.body.fileImage,
            decription: req.body.txtDescription,
            price: req.body.txtPrice,
            unit: req.body.txtUnit,
            size: req.body.txtSize,
            content: req.body.txtContent,
            adress: req.body.txtAdress,
            active: req.body.active,
            numberbath: req.body.txtBath,
            numberbed: req.body.txtBad,
            name_user: req.body.txtName_user,
            phone: req.body.txtPhone,
            id_post: req.body.id_post

        }).then(console.log('thanh cong')).catch((err) => console.log(err))
    }
}

module.exports.delete = async (req, res) => {
    await Detail.findByIdAndDelete(req.body.id).then(console.log('Deleted')).catch(err => console.log(err))
}